const express = require('express')
const dotenv = require('dotenv')
const colors = require('colors')
// const cors = require('cors')
// const cookieParser = require('cookie-parser')
const connectDB = require('./config/db')

const app = express()
dotenv.config()
connectDB()

// app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
// app.use(cookieParser())

app.get('/', (req, res) => res.send('Hello World'))

const port = process.env.PORT || 5000

app.listen(port, () => {
    console.log(`Server ishga tushdi. Port ${port}`.yellow.bold)
})

// export default app
module.exports = app
